/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Hello from './src/screens/HomeScreen'; 
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
  
const navigator = createStackNavigator  ({
  Hellos : Hello,
},
{
  initialRouterName :'Hellos',
  defaultNavigationOptions : {
    title : 'App'
  }
}
)

export default createAppContainer(navigator);
  
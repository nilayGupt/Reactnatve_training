/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
import React from 'react';
import {Text, StyleSheet, View} from 'react-native';

const Hello = () => {
  const greeting = 'Hello';
  return (
    <View
      style={{
        color: 'yellow',
      }}>
      <Text style={Style.textStyle}> Hello this is my first app </Text>
      <Text>{greeting}</Text>
    </View>
  );
};
const Style = StyleSheet.create({
  textStyle: {
    fontSize: 30,
    color: 'red',
    justifyContent: 'center',
  },
});

export default Hello;
